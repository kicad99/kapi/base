import * as $protobuf from "protobufjs";
/** Namespace base. */
export namespace base {

    /** Properties of a PingReq. */
    interface IPingReq {

        /** PingReq Code */
        Code?: (number|null);

        /** PingReq RequestTime */
        RequestTime?: (boolean|null);

        /** PingReq PongTime */
        PongTime?: (Long|null);

        /** PingReq SessionID */
        SessionID?: (Uint8Array|null);
    }

    /** Represents a PingReq. */
    class PingReq implements IPingReq {

        /**
         * Constructs a new PingReq.
         * @param [properties] Properties to set
         */
        constructor(properties?: base.IPingReq);

        /** PingReq Code. */
        public Code: number;

        /** PingReq RequestTime. */
        public RequestTime: boolean;

        /** PingReq PongTime. */
        public PongTime: Long;

        /** PingReq SessionID. */
        public SessionID: Uint8Array;

        /**
         * Encodes the specified PingReq message. Does not implicitly {@link base.PingReq.verify|verify} messages.
         * @param message PingReq message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: base.IPingReq, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a PingReq message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns PingReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): base.PingReq;
    }
}
