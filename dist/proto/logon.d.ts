import * as $protobuf from "protobufjs";
/** Namespace base. */
export namespace base {

    /** ClientType enum. */
    enum ClientType {
        UNKNOWN = 0,
        WEBCLIENT = 1,
        ANDROIDCLIENT = 2,
        IOSCLIENT = 3,
        PCCLIENT = 4,
        EMBEDCLIENT = 5
    }

    /** Properties of a ClientInfo. */
    interface IClientInfo {

        /** ClientInfo Model */
        Model?: (string|null);

        /** ClientInfo Version */
        Version?: (string|null);

        /** ClientInfo Channel */
        Channel?: (string|null);
    }

    /** Represents a ClientInfo. */
    class ClientInfo implements IClientInfo {

        /**
         * Constructs a new ClientInfo.
         * @param [properties] Properties to set
         */
        constructor(properties?: base.IClientInfo);

        /** ClientInfo Model. */
        public Model: string;

        /** ClientInfo Version. */
        public Version: string;

        /** ClientInfo Channel. */
        public Channel: string;

        /**
         * Encodes the specified ClientInfo message. Does not implicitly {@link base.ClientInfo.verify|verify} messages.
         * @param message ClientInfo message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: base.IClientInfo, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a ClientInfo message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ClientInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): base.ClientInfo;
    }

    /** Properties of a LoginReq. */
    interface ILoginReq {

        /** LoginReq Sys */
        Sys?: (string|null);

        /** LoginReq UserName */
        UserName?: (string|null);

        /** LoginReq PassHash */
        PassHash?: (Uint8Array|null);

        /** LoginReq PassHashMethod */
        PassHashMethod?: (number|null);

        /** LoginReq Salt */
        Salt?: (string|null);

        /** LoginReq ClientTime */
        ClientTime?: (Long|null);

        /** LoginReq ClientType */
        ClientType?: (base.ClientType|null);

        /** LoginReq ClientInfo */
        ClientInfo?: (base.IClientInfo|null);
    }

    /** Represents a LoginReq. */
    class LoginReq implements ILoginReq {

        /**
         * Constructs a new LoginReq.
         * @param [properties] Properties to set
         */
        constructor(properties?: base.ILoginReq);

        /** LoginReq Sys. */
        public Sys: string;

        /** LoginReq UserName. */
        public UserName: string;

        /** LoginReq PassHash. */
        public PassHash: Uint8Array;

        /** LoginReq PassHashMethod. */
        public PassHashMethod: number;

        /** LoginReq Salt. */
        public Salt: string;

        /** LoginReq ClientTime. */
        public ClientTime: Long;

        /** LoginReq ClientType. */
        public ClientType: base.ClientType;

        /** LoginReq ClientInfo. */
        public ClientInfo?: (base.IClientInfo|null);

        /**
         * Encodes the specified LoginReq message. Does not implicitly {@link base.LoginReq.verify|verify} messages.
         * @param message LoginReq message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: base.ILoginReq, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a LoginReq message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns LoginReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): base.LoginReq;
    }

    /** LogonRespCode enum. */
    enum LogonRespCode {
        Success = 0,
        SysErr = 1,
        UserNameErr = 2,
        PassErr = 3,
        PassHashMethodErr = 4,
        SessionIDErr = 5,
        ClientTimeErr = 6,
        UnknownErr = 15
    }

    /** Properties of a LoginResp. */
    interface ILoginResp {

        /** LoginResp Code */
        Code?: (base.LogonRespCode|null);

        /** LoginResp SessionID */
        SessionID?: (Uint8Array|null);

        /** LoginResp Info */
        Info?: (string|null);
    }

    /** Represents a LoginResp. */
    class LoginResp implements ILoginResp {

        /**
         * Constructs a new LoginResp.
         * @param [properties] Properties to set
         */
        constructor(properties?: base.ILoginResp);

        /** LoginResp Code. */
        public Code: base.LogonRespCode;

        /** LoginResp SessionID. */
        public SessionID: Uint8Array;

        /** LoginResp Info. */
        public Info: string;

        /**
         * Encodes the specified LoginResp message. Does not implicitly {@link base.LoginResp.verify|verify} messages.
         * @param message LoginResp message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: base.ILoginResp, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a LoginResp message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns LoginResp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): base.LoginResp;
    }

    /** Properties of a LogoutReq. */
    interface ILogoutReq {

        /** LogoutReq Sys */
        Sys?: (string|null);

        /** LogoutReq UserName */
        UserName?: (string|null);

        /** LogoutReq SessionID */
        SessionID?: (Uint8Array|null);
    }

    /** Represents a LogoutReq. */
    class LogoutReq implements ILogoutReq {

        /**
         * Constructs a new LogoutReq.
         * @param [properties] Properties to set
         */
        constructor(properties?: base.ILogoutReq);

        /** LogoutReq Sys. */
        public Sys: string;

        /** LogoutReq UserName. */
        public UserName: string;

        /** LogoutReq SessionID. */
        public SessionID: Uint8Array;

        /**
         * Encodes the specified LogoutReq message. Does not implicitly {@link base.LogoutReq.verify|verify} messages.
         * @param message LogoutReq message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: base.ILogoutReq, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a LogoutReq message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns LogoutReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): base.LogoutReq;
    }

    /** Properties of a LogoutResp. */
    interface ILogoutResp {

        /** LogoutResp Code */
        Code?: (base.LogonRespCode|null);
    }

    /** Represents a LogoutResp. */
    class LogoutResp implements ILogoutResp {

        /**
         * Constructs a new LogoutResp.
         * @param [properties] Properties to set
         */
        constructor(properties?: base.ILogoutResp);

        /** LogoutResp Code. */
        public Code: base.LogonRespCode;

        /**
         * Encodes the specified LogoutResp message. Does not implicitly {@link base.LogoutResp.verify|verify} messages.
         * @param message LogoutResp message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: base.ILogoutResp, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a LogoutResp message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns LogoutResp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): base.LogoutResp;
    }
}
