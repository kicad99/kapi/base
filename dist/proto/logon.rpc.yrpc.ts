import {Writer} from 'protobufjs'
import {rpcCon} from 'jsyrpc'
import {ICallOption} from 'jsyrpc'
import * as LogonY from '@ygen/logon'
import * as PingY from '@ygen/ping'


export function LogonLogin(req:LogonY.base.ILoginReq,callOpt?:ICallOption):boolean{
	let w:Writer=LogonY.base.LoginReq.encode(req)
	let reqData=w.finish()
	
	const api='/base.Logon/Login'
	const v=0

	return rpcCon.UnaryCall(reqData,api,v,LogonY.base.LoginResp,callOpt)
}export function LogonLogout(req:LogonY.base.ILogoutReq,callOpt?:ICallOption):boolean{
	let w:Writer=LogonY.base.LogoutReq.encode(req)
	let reqData=w.finish()
	
	const api='/base.Logon/Logout'
	const v=0

	return rpcCon.UnaryCall(reqData,api,v,LogonY.base.LogoutResp,callOpt)
}export function LogonPing(req:PingY.base.IPingReq,callOpt?:ICallOption):boolean{
	let w:Writer=PingY.base.PingReq.encode(req)
	let reqData=w.finish()
	
	const api='/base.Logon/Ping'
	const v=0

	return rpcCon.UnaryCall(reqData,api,v,PingY.base.PingReq,callOpt)
}



export class Logon { 
 public static Login(req:LogonY.base.ILoginReq,callOpt?:ICallOption):boolean{
	return LogonLogin(req, callOpt)	
}
 public static Logout(req:LogonY.base.ILogoutReq,callOpt?:ICallOption):boolean{
	return LogonLogout(req, callOpt)	
}
 public static Ping(req:PingY.base.IPingReq,callOpt?:ICallOption):boolean{
	return LogonPing(req, callOpt)	
}
}
export default {
  Logon,
  LogonLogin,
  LogonLogout,
  LogonPing,
}
