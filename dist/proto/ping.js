/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
import * as $protobuf from "protobufjs/minimal";

// Common aliases
const $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
const $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

export const base = $root.base = (() => {

    /**
     * Namespace base.
     * @exports base
     * @namespace
     */
    const base = $root.base || {};

    base.PingReq = (function() {

        /**
         * Properties of a PingReq.
         * @memberof base
         * @interface IPingReq
         * @property {number|null} [Code] PingReq Code
         * @property {boolean|null} [RequestTime] PingReq RequestTime
         * @property {Long|null} [PongTime] PingReq PongTime
         * @property {Uint8Array|null} [SessionID] PingReq SessionID
         */

        /**
         * Constructs a new PingReq.
         * @memberof base
         * @classdesc Represents a PingReq.
         * @implements IPingReq
         * @constructor
         * @param {base.IPingReq=} [properties] Properties to set
         */
        function PingReq(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * PingReq Code.
         * @member {number} Code
         * @memberof base.PingReq
         * @instance
         */
        PingReq.prototype.Code = 0;

        /**
         * PingReq RequestTime.
         * @member {boolean} RequestTime
         * @memberof base.PingReq
         * @instance
         */
        PingReq.prototype.RequestTime = false;

        /**
         * PingReq PongTime.
         * @member {Long} PongTime
         * @memberof base.PingReq
         * @instance
         */
        PingReq.prototype.PongTime = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        /**
         * PingReq SessionID.
         * @member {Uint8Array} SessionID
         * @memberof base.PingReq
         * @instance
         */
        PingReq.prototype.SessionID = $util.newBuffer([]);

        /**
         * Encodes the specified PingReq message. Does not implicitly {@link base.PingReq.verify|verify} messages.
         * @function encode
         * @memberof base.PingReq
         * @static
         * @param {base.IPingReq} message PingReq message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        PingReq.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.Code != null && Object.hasOwnProperty.call(message, "Code"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.Code);
            if (message.RequestTime != null && Object.hasOwnProperty.call(message, "RequestTime"))
                writer.uint32(/* id 2, wireType 0 =*/16).bool(message.RequestTime);
            if (message.PongTime != null && Object.hasOwnProperty.call(message, "PongTime"))
                writer.uint32(/* id 3, wireType 0 =*/24).int64(message.PongTime);
            if (message.SessionID != null && Object.hasOwnProperty.call(message, "SessionID"))
                writer.uint32(/* id 4, wireType 2 =*/34).bytes(message.SessionID);
            return writer;
        };

        /**
         * Decodes a PingReq message from the specified reader or buffer.
         * @function decode
         * @memberof base.PingReq
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {base.PingReq} PingReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        PingReq.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.base.PingReq();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.Code = reader.int32();
                    break;
                case 2:
                    message.RequestTime = reader.bool();
                    break;
                case 3:
                    message.PongTime = reader.int64();
                    break;
                case 4:
                    message.SessionID = reader.bytes();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        return PingReq;
    })();

    return base;
})();

export { $root as default };
