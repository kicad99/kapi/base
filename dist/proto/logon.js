/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
import * as $protobuf from "protobufjs/minimal";

// Common aliases
const $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
const $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

export const base = $root.base = (() => {

    /**
     * Namespace base.
     * @exports base
     * @namespace
     */
    const base = $root.base || {};

    /**
     * ClientType enum.
     * @name base.ClientType
     * @enum {number}
     * @property {number} UNKNOWN=0 UNKNOWN value
     * @property {number} WEBCLIENT=1 WEBCLIENT value
     * @property {number} ANDROIDCLIENT=2 ANDROIDCLIENT value
     * @property {number} IOSCLIENT=3 IOSCLIENT value
     * @property {number} PCCLIENT=4 PCCLIENT value
     * @property {number} EMBEDCLIENT=5 EMBEDCLIENT value
     */
    base.ClientType = (function() {
        const valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "UNKNOWN"] = 0;
        values[valuesById[1] = "WEBCLIENT"] = 1;
        values[valuesById[2] = "ANDROIDCLIENT"] = 2;
        values[valuesById[3] = "IOSCLIENT"] = 3;
        values[valuesById[4] = "PCCLIENT"] = 4;
        values[valuesById[5] = "EMBEDCLIENT"] = 5;
        return values;
    })();

    base.ClientInfo = (function() {

        /**
         * Properties of a ClientInfo.
         * @memberof base
         * @interface IClientInfo
         * @property {string|null} [Model] ClientInfo Model
         * @property {string|null} [Version] ClientInfo Version
         * @property {string|null} [Channel] ClientInfo Channel
         */

        /**
         * Constructs a new ClientInfo.
         * @memberof base
         * @classdesc Represents a ClientInfo.
         * @implements IClientInfo
         * @constructor
         * @param {base.IClientInfo=} [properties] Properties to set
         */
        function ClientInfo(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * ClientInfo Model.
         * @member {string} Model
         * @memberof base.ClientInfo
         * @instance
         */
        ClientInfo.prototype.Model = "";

        /**
         * ClientInfo Version.
         * @member {string} Version
         * @memberof base.ClientInfo
         * @instance
         */
        ClientInfo.prototype.Version = "";

        /**
         * ClientInfo Channel.
         * @member {string} Channel
         * @memberof base.ClientInfo
         * @instance
         */
        ClientInfo.prototype.Channel = "";

        /**
         * Encodes the specified ClientInfo message. Does not implicitly {@link base.ClientInfo.verify|verify} messages.
         * @function encode
         * @memberof base.ClientInfo
         * @static
         * @param {base.IClientInfo} message ClientInfo message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        ClientInfo.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.Model != null && Object.hasOwnProperty.call(message, "Model"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.Model);
            if (message.Version != null && Object.hasOwnProperty.call(message, "Version"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.Version);
            if (message.Channel != null && Object.hasOwnProperty.call(message, "Channel"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.Channel);
            return writer;
        };

        /**
         * Decodes a ClientInfo message from the specified reader or buffer.
         * @function decode
         * @memberof base.ClientInfo
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {base.ClientInfo} ClientInfo
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        ClientInfo.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.base.ClientInfo();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.Model = reader.string();
                    break;
                case 2:
                    message.Version = reader.string();
                    break;
                case 3:
                    message.Channel = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        return ClientInfo;
    })();

    base.LoginReq = (function() {

        /**
         * Properties of a LoginReq.
         * @memberof base
         * @interface ILoginReq
         * @property {string|null} [Sys] LoginReq Sys
         * @property {string|null} [UserName] LoginReq UserName
         * @property {Uint8Array|null} [PassHash] LoginReq PassHash
         * @property {number|null} [PassHashMethod] LoginReq PassHashMethod
         * @property {string|null} [Salt] LoginReq Salt
         * @property {Long|null} [ClientTime] LoginReq ClientTime
         * @property {base.ClientType|null} [ClientType] LoginReq ClientType
         * @property {base.IClientInfo|null} [ClientInfo] LoginReq ClientInfo
         */

        /**
         * Constructs a new LoginReq.
         * @memberof base
         * @classdesc Represents a LoginReq.
         * @implements ILoginReq
         * @constructor
         * @param {base.ILoginReq=} [properties] Properties to set
         */
        function LoginReq(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * LoginReq Sys.
         * @member {string} Sys
         * @memberof base.LoginReq
         * @instance
         */
        LoginReq.prototype.Sys = "";

        /**
         * LoginReq UserName.
         * @member {string} UserName
         * @memberof base.LoginReq
         * @instance
         */
        LoginReq.prototype.UserName = "";

        /**
         * LoginReq PassHash.
         * @member {Uint8Array} PassHash
         * @memberof base.LoginReq
         * @instance
         */
        LoginReq.prototype.PassHash = $util.newBuffer([]);

        /**
         * LoginReq PassHashMethod.
         * @member {number} PassHashMethod
         * @memberof base.LoginReq
         * @instance
         */
        LoginReq.prototype.PassHashMethod = 0;

        /**
         * LoginReq Salt.
         * @member {string} Salt
         * @memberof base.LoginReq
         * @instance
         */
        LoginReq.prototype.Salt = "";

        /**
         * LoginReq ClientTime.
         * @member {Long} ClientTime
         * @memberof base.LoginReq
         * @instance
         */
        LoginReq.prototype.ClientTime = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        /**
         * LoginReq ClientType.
         * @member {base.ClientType} ClientType
         * @memberof base.LoginReq
         * @instance
         */
        LoginReq.prototype.ClientType = 0;

        /**
         * LoginReq ClientInfo.
         * @member {base.IClientInfo|null|undefined} ClientInfo
         * @memberof base.LoginReq
         * @instance
         */
        LoginReq.prototype.ClientInfo = null;

        /**
         * Encodes the specified LoginReq message. Does not implicitly {@link base.LoginReq.verify|verify} messages.
         * @function encode
         * @memberof base.LoginReq
         * @static
         * @param {base.ILoginReq} message LoginReq message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        LoginReq.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.Sys != null && Object.hasOwnProperty.call(message, "Sys"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.Sys);
            if (message.UserName != null && Object.hasOwnProperty.call(message, "UserName"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.UserName);
            if (message.PassHash != null && Object.hasOwnProperty.call(message, "PassHash"))
                writer.uint32(/* id 3, wireType 2 =*/26).bytes(message.PassHash);
            if (message.PassHashMethod != null && Object.hasOwnProperty.call(message, "PassHashMethod"))
                writer.uint32(/* id 4, wireType 0 =*/32).int32(message.PassHashMethod);
            if (message.Salt != null && Object.hasOwnProperty.call(message, "Salt"))
                writer.uint32(/* id 5, wireType 2 =*/42).string(message.Salt);
            if (message.ClientTime != null && Object.hasOwnProperty.call(message, "ClientTime"))
                writer.uint32(/* id 6, wireType 0 =*/48).int64(message.ClientTime);
            if (message.ClientType != null && Object.hasOwnProperty.call(message, "ClientType"))
                writer.uint32(/* id 7, wireType 0 =*/56).int32(message.ClientType);
            if (message.ClientInfo != null && Object.hasOwnProperty.call(message, "ClientInfo"))
                $root.base.ClientInfo.encode(message.ClientInfo, writer.uint32(/* id 8, wireType 2 =*/66).fork()).ldelim();
            return writer;
        };

        /**
         * Decodes a LoginReq message from the specified reader or buffer.
         * @function decode
         * @memberof base.LoginReq
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {base.LoginReq} LoginReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        LoginReq.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.base.LoginReq();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.Sys = reader.string();
                    break;
                case 2:
                    message.UserName = reader.string();
                    break;
                case 3:
                    message.PassHash = reader.bytes();
                    break;
                case 4:
                    message.PassHashMethod = reader.int32();
                    break;
                case 5:
                    message.Salt = reader.string();
                    break;
                case 6:
                    message.ClientTime = reader.int64();
                    break;
                case 7:
                    message.ClientType = reader.int32();
                    break;
                case 8:
                    message.ClientInfo = $root.base.ClientInfo.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        return LoginReq;
    })();

    /**
     * LogonRespCode enum.
     * @name base.LogonRespCode
     * @enum {number}
     * @property {number} Success=0 Success value
     * @property {number} SysErr=1 SysErr value
     * @property {number} UserNameErr=2 UserNameErr value
     * @property {number} PassErr=3 PassErr value
     * @property {number} PassHashMethodErr=4 PassHashMethodErr value
     * @property {number} SessionIDErr=5 SessionIDErr value
     * @property {number} ClientTimeErr=6 ClientTimeErr value
     * @property {number} UnknownErr=15 UnknownErr value
     */
    base.LogonRespCode = (function() {
        const valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "Success"] = 0;
        values[valuesById[1] = "SysErr"] = 1;
        values[valuesById[2] = "UserNameErr"] = 2;
        values[valuesById[3] = "PassErr"] = 3;
        values[valuesById[4] = "PassHashMethodErr"] = 4;
        values[valuesById[5] = "SessionIDErr"] = 5;
        values[valuesById[6] = "ClientTimeErr"] = 6;
        values[valuesById[15] = "UnknownErr"] = 15;
        return values;
    })();

    base.LoginResp = (function() {

        /**
         * Properties of a LoginResp.
         * @memberof base
         * @interface ILoginResp
         * @property {base.LogonRespCode|null} [Code] LoginResp Code
         * @property {Uint8Array|null} [SessionID] LoginResp SessionID
         * @property {string|null} [Info] LoginResp Info
         */

        /**
         * Constructs a new LoginResp.
         * @memberof base
         * @classdesc Represents a LoginResp.
         * @implements ILoginResp
         * @constructor
         * @param {base.ILoginResp=} [properties] Properties to set
         */
        function LoginResp(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * LoginResp Code.
         * @member {base.LogonRespCode} Code
         * @memberof base.LoginResp
         * @instance
         */
        LoginResp.prototype.Code = 0;

        /**
         * LoginResp SessionID.
         * @member {Uint8Array} SessionID
         * @memberof base.LoginResp
         * @instance
         */
        LoginResp.prototype.SessionID = $util.newBuffer([]);

        /**
         * LoginResp Info.
         * @member {string} Info
         * @memberof base.LoginResp
         * @instance
         */
        LoginResp.prototype.Info = "";

        /**
         * Encodes the specified LoginResp message. Does not implicitly {@link base.LoginResp.verify|verify} messages.
         * @function encode
         * @memberof base.LoginResp
         * @static
         * @param {base.ILoginResp} message LoginResp message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        LoginResp.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.Code != null && Object.hasOwnProperty.call(message, "Code"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.Code);
            if (message.SessionID != null && Object.hasOwnProperty.call(message, "SessionID"))
                writer.uint32(/* id 2, wireType 2 =*/18).bytes(message.SessionID);
            if (message.Info != null && Object.hasOwnProperty.call(message, "Info"))
                writer.uint32(/* id 15, wireType 2 =*/122).string(message.Info);
            return writer;
        };

        /**
         * Decodes a LoginResp message from the specified reader or buffer.
         * @function decode
         * @memberof base.LoginResp
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {base.LoginResp} LoginResp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        LoginResp.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.base.LoginResp();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.Code = reader.int32();
                    break;
                case 2:
                    message.SessionID = reader.bytes();
                    break;
                case 15:
                    message.Info = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        return LoginResp;
    })();

    base.LogoutReq = (function() {

        /**
         * Properties of a LogoutReq.
         * @memberof base
         * @interface ILogoutReq
         * @property {string|null} [Sys] LogoutReq Sys
         * @property {string|null} [UserName] LogoutReq UserName
         * @property {Uint8Array|null} [SessionID] LogoutReq SessionID
         */

        /**
         * Constructs a new LogoutReq.
         * @memberof base
         * @classdesc Represents a LogoutReq.
         * @implements ILogoutReq
         * @constructor
         * @param {base.ILogoutReq=} [properties] Properties to set
         */
        function LogoutReq(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * LogoutReq Sys.
         * @member {string} Sys
         * @memberof base.LogoutReq
         * @instance
         */
        LogoutReq.prototype.Sys = "";

        /**
         * LogoutReq UserName.
         * @member {string} UserName
         * @memberof base.LogoutReq
         * @instance
         */
        LogoutReq.prototype.UserName = "";

        /**
         * LogoutReq SessionID.
         * @member {Uint8Array} SessionID
         * @memberof base.LogoutReq
         * @instance
         */
        LogoutReq.prototype.SessionID = $util.newBuffer([]);

        /**
         * Encodes the specified LogoutReq message. Does not implicitly {@link base.LogoutReq.verify|verify} messages.
         * @function encode
         * @memberof base.LogoutReq
         * @static
         * @param {base.ILogoutReq} message LogoutReq message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        LogoutReq.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.Sys != null && Object.hasOwnProperty.call(message, "Sys"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.Sys);
            if (message.UserName != null && Object.hasOwnProperty.call(message, "UserName"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.UserName);
            if (message.SessionID != null && Object.hasOwnProperty.call(message, "SessionID"))
                writer.uint32(/* id 3, wireType 2 =*/26).bytes(message.SessionID);
            return writer;
        };

        /**
         * Decodes a LogoutReq message from the specified reader or buffer.
         * @function decode
         * @memberof base.LogoutReq
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {base.LogoutReq} LogoutReq
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        LogoutReq.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.base.LogoutReq();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.Sys = reader.string();
                    break;
                case 2:
                    message.UserName = reader.string();
                    break;
                case 3:
                    message.SessionID = reader.bytes();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        return LogoutReq;
    })();

    base.LogoutResp = (function() {

        /**
         * Properties of a LogoutResp.
         * @memberof base
         * @interface ILogoutResp
         * @property {base.LogonRespCode|null} [Code] LogoutResp Code
         */

        /**
         * Constructs a new LogoutResp.
         * @memberof base
         * @classdesc Represents a LogoutResp.
         * @implements ILogoutResp
         * @constructor
         * @param {base.ILogoutResp=} [properties] Properties to set
         */
        function LogoutResp(properties) {
            if (properties)
                for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * LogoutResp Code.
         * @member {base.LogonRespCode} Code
         * @memberof base.LogoutResp
         * @instance
         */
        LogoutResp.prototype.Code = 0;

        /**
         * Encodes the specified LogoutResp message. Does not implicitly {@link base.LogoutResp.verify|verify} messages.
         * @function encode
         * @memberof base.LogoutResp
         * @static
         * @param {base.ILogoutResp} message LogoutResp message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        LogoutResp.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.Code != null && Object.hasOwnProperty.call(message, "Code"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.Code);
            return writer;
        };

        /**
         * Decodes a LogoutResp message from the specified reader or buffer.
         * @function decode
         * @memberof base.LogoutResp
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {base.LogoutResp} LogoutResp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        LogoutResp.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            let end = length === undefined ? reader.len : reader.pos + length, message = new $root.base.LogoutResp();
            while (reader.pos < end) {
                let tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.Code = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        return LogoutResp;
    })();

    return base;
})();

export { $root as default };
