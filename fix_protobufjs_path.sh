#!/usr/bin/env bash

old="push((config.es6 ? \"const\" : \"var\") + \" \" + escapeName(ns.name) + \" = {};\");"
new="push((config.es6 ? \"const\" : \"var\") + \" \" + escapeName(ns.name) + \" = \$root.\" + escapeName(ns.name) + \" || {};\");"

target="node_modules/protobufjs/cli/targets/static.js"

sed -i "s/$old/$new/g" $target
