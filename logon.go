package base

import (
	"crypto/sha256"
	"encoding/hex"
	"strconv"
	"strings"
)

// LoginHashDB 保存到数据库的Hash值
func LoginHashDB(sys, username, passwd string) string {
	hash256 := sha256.Sum256([]byte(sys + username + passwd))
	hexUpper := strings.ToUpper(hex.EncodeToString(hash256[:]))
	return hexUpper
}

// LoginHash0 登录检验方式0计算Hash值
func LoginHash0(sys, username, passwd string, clientTime int64) []byte {
	hexUpper := LoginHashDB(sys, username, passwd)

	return LoginHash0DB(hexUpper, clientTime)
}

// LoginHash0DB 使用预先计算的Hash值来计算登录校验
func LoginHash0DB(sysUsernamePasswdHash string, clientTime int64) []byte {
	timeHex := strings.ToUpper(strconv.FormatInt(clientTime, 16))
	hash := sha256.Sum256([]byte(sysUsernamePasswdHash + timeHex))
	//resultHex := strings.ToUpper(hex.EncodeToString(hash[:]))
	//_ = resultHex
	return hash[:]
}
