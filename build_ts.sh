#!/usr/bin/env bash

# 重置工作目录
cd "$(dirname "$0")"

./fix_protobufjs_path.sh

#pbjs & pbts path
bin=node_modules/.bin

#设置proto协议文件路径变量
protoPath=proto

#创建编译输出目录
outputPath=dist
generateOutputPath(){
    out=$outputPath
    path=$1
    if [ "$path" != "" ]; then
        out="$out/$path"
    fi

    mkdir -p $out
    echo "$out"
}

#$1需要编译的文件，$2为文件路径
buildProto(){
    src=$1
    out=$(generateOutputPath $src)
    file=$2
    filename=${file%\.*}
    echo "Building file: $src/$file, output: $out"

    $bin/pbjs --no-create --no-verify --no-convert --no-delimited \
        --force-long -t static-module -w es6 \
        -p $protoPath -p . \
        -o $out/$filename.js $src/$file
    $bin/pbts -o $out/$filename.d.ts $out/$filename.js
}

#生成rpc静态模块方法
buildRpcProto(){
    src=$1
    out=$(generateOutputPath $src)
    file=$2
    # filename=${file%\.*}
    echo "Building file: $src/$file, output: $out"

    protoc --proto_path=$protoPath --proto_path=. \
    --ygen-jsyrpc_out=scope=module1:$out $src/$file

    #官方标准编译
    # protoc --proto_path=$protoPath --proto_path=. \
    # --js_out=import_style=commonjs:$out \
    # --grpc-web_out=import_style=commonjs,mode=grpcwebtext:$out $src/$file
}

# 递归proto文件夹, 编译所有proto文件
readDir(){
    for p in `ls $1`
    do
        #递归读取目录
        if [ -d "$1/$p" ]
        then
            readDir "$1/$p"
        #.rpc.proto协议文件
        elif [ $(echo $p | grep -e ".*\.rpc\.proto") ]
        then
            buildRpcProto $1 $p
        #非.rpc.proto协议文件
        elif [ $(echo $p | grep -e ".*[^(\.rpc)]\.proto") ]
        then
            buildProto $1 $p
        fi
    done
}

readDir $protoPath

# # 添加缺失的Long类型引用
# addMissLongType(){
#     sed -i '1s;^;import { Long } from "protobufjs"\;\n;' $outputPath/$1
# }

# # bys.api.proto/yrpcmsg 使用了Long类型
# addMissLongType bys.api.d.ts
# addMissLongType yrpcmsg.d.ts

# #使用prettier格式化生成的文件
# node ./node_modules/prettier/bin-prettier.js $outputPath/*  --write --no-semi --single-quote  --trailing-comma all

#将生成的文件添加到git中
if [ -d "$outputPath" ]; then
    git add $outputPath
fi
